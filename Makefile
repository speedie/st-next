# st - simple terminal
# See LICENSE file for copyright and license details.
.POSIX:

include options.mk

SRC = st.c x.c boxdraw.c hb.c
OBJ = $(SRC:.c=.o)

all: options st

options:
	@echo st build options:
	@echo "CFLAGS  = $(STCFLAGS)"
	@echo "LDFLAGS = $(STLDFLAGS)"
	@echo "CC      = $(CC)"
	@echo "======================"

.c.o:
	$(CC) $(STCFLAGS) -c $<

st.o: options.h st.h win.h
x.o: arg.h options.h st.h win.h
boxdraw.o: options.h st.h boxdraw_data.h
hb.o: st.h

$(OBJ): options.h options.mk

st: $(OBJ)
	$(CC) -o $@ $(OBJ) $(STLDFLAGS)

clean:
	rm -f st $(OBJ) st-$(VERSION).tar.gz

dist: clean
	mkdir -p st-$(VERSION)
	cp -R LICENSE Makefile README *.mk\
		 *.h *.info *.c *.png *.desktop *.ttf docs/ scripts/ \
		st-$(VERSION)
	tar -cf - st-$(VERSION) | gzip > st-$(VERSION).tar.gz
	rm -rf st-$(VERSION)

install: st
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f st $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/st
	tic -sx st.info
	rm -f *.o st
	cp -f scripts/st_urllist $(DESTDIR)$(PREFIX)/bin
	cp -f scripts/st_buffer $(DESTDIR)$(PREFIX)/bin
	cp -f scripts/st_xurls $(DESTDIR)$(PREFIX)/bin
	cp -f scripts/st_help $(DESTDIR)$(PREFIX)/bin
	chmod +x $(DESTDIR)$(PREFIX)/bin/st_urllist
	chmod +x $(DESTDIR)$(PREFIX)/bin/st_buffer
	chmod +x $(DESTDIR)$(PREFIX)/bin/st_xurls
	chmod +x $(DESTDIR)$(PREFIX)/bin/st_help
	mkdir -p $(DESTDIR)$(ICONPREFIX)
	[ -f $(ICONNAME) ] && cp -f $(ICONNAME) $(DESTDIR)$(ICONPREFIX) || :

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/st
	rm -f $(DESTDIR)$(MANPREFIX)/man1/st.1
	rm -f $(DESTDIR)$(ICONPREFIX)/$(ICONNAME)

help:
	@echo "st Makefile help"
	@echo
	@echo "install    Install st."
	@echo "uninstall  Uninstall st."
	@echo "clean      Remove st tarball and binary"
	@echo "options    Print compilation options."
	@echo "enablefont Install the font.ttf using fontctrl."
	@echo "dist       Create a tarball for use with package managers or for releases."
	@echo "help       List of all options."

enablefont:
	fontctrl install font.ttf --global
	fontctrl enable font.ttf --global

.PHONY: all options clean dist install uninstall enablefont help
