/* This header file contains the keybinds available to the user.
 * This is not a complete list, the rest can be changed in array.h and mouse.h respectively.
 *
 * CONTROL = Left Control/Ctrl key
 * SHIFT   = Left Shift key
 * ALT     = Left Alt key
 * ALTR    = Right Alt key
 * SUPER   = Left Super/Windows/Command key
 * SUPERR  = Right Super/Windows/Command key
 * ANY     = Any of the above modifiers
 * NONE    = No modifier at all
 */

static Shortcut shortcuts[] = {
	/* modifier             keysym          function        argument */
	{ NONE,                 XK_Break,       sendbreak,      {.i =  0} },
	{ CONTROL,              XK_Print,       toggleprinter,  {.i =  0} },
	{ SHIFT,                XK_Print,       printscreen,    {.i =  0} },
	{ NONE,                 XK_Print,       printsel,       {.i =  0} },
	{ CONTROL,              XK_equal,       zoom,           {.f = +1} },
	{ CONTROL,              XK_minus,       zoom,           {.f = -1} },
	{ CONTROL,              XK_0,           zoomreset,      {.f =  0} },
	{ CONTROL,              XK_y,           clipcopy,       {.i =  0} },
	{ CONTROL,              XK_p,           clippaste,      {.i =  0} },
	{ CONTROL|SHIFT,        XK_Num_Lock,    numlock,        {.i =  0} },
    { CONTROL|SHIFT,        XK_Escape,      keyboard_select,{.i =   0} },
	{ CONTROL|SHIFT,        XK_C,           kexec,          {.scmd = "clear" } },
	{ CONTROL|SHIFT,        XK_H,           kexec,          {.scmd = "st_help" } },
	{ CONTROL|SHIFT,        XK_E,           kexec,          {.scmd = "$EDITOR" } },
	{ CONTROL|SHIFT,        XK_D,           kexec,          {.scmd = "$PERM !!" } },
	{ CONTROL|SHIFT,        XK_K,           kscrollup,      {.i = +1} },
	{ CONTROL|SHIFT,        XK_J,           kscrolldown,    {.i = +1} },
	{ CONTROL|SHIFT,	    XK_U,           externalpipeout,{.v = listurl } },
};
