/* speedie.gq's build of suckless.org's simple terminal (st).
 *
 * This is a fork of suckless's st, a minimal in design, just works terminal emulator that by default does nothing more.
 * Note that this terminal has a list of fonts (See 'Font options') that must be changed before installing.
 * 
 * Additionally, if you use OpenBSD or FreeBSD, you must edit 'config.mk'.
 * If you use GNU/Linux, you should be able to recompile st.
 *
 * To find out what to enter in the font and font2 chars, run 'fc-list | grep <fontname>'.
 * Font options
 */
static char *font                      = "DejaVuSansMono Nerd Font:style=Bold:pixelsize=12:antialias=true:autohint=true"; /* Main font to use */
/* This font should be bundled with st. Install it manually or with fontctrl (available here: https://codeberg.org/speedie/fontctrl)
 * If it is not available, you may need to 'fontctrl install <font file> --global'.
 */

/* Secondary fonts
 * 
 * These will be picked if the *font does not exist, or doesn't contain the glyphs necessary.
 * You can add multiple fonts if you wish.
 * 
 * For best compatibility, have one Emoji font, one regular font, one Powerline font and one Nerd font.
 * If you don't need these, you can of course omit them.
 */
static char *font2[]                   = { "DejaVu Sans Mono:pixelsize=12:antialias=true:autohint=true",
										   "Noto Color Emoji:pixelsize=12:antialias=true:autohint=true", 
										   "fontawesome:pixelsize=12:antialias=true:autohint=true",
};

/* Window options */
static int borderpx   = 0; /* Size of a small border around the text */
int allowaltscreen    = 1; /* Allow alt screen */
int allowwindowops    = 1; /* Allow non-interactive, insecure window operations such as setting the clipboard text */
int windowdecorations = 1; /* Use window decorations */

/* Text drawing options */
int boldbright           = 0; /* Draw bold text using bright colors */
static unsigned int cols = 80; /* Number of columns */
static unsigned int rows = 24; /* Number of rows */
wchar_t *worddelimiters = L" ";

/* Text latency options */
static uint synctimeout = 200; /* Time before refreshing */
static double minlatency = 8; /* Minimum latency */
static double maxlatency = 33; /* Maximum latency */

/* Mouse options */
static unsigned int doubleclicktimeout = 300;
static unsigned int tripleclicktimeout = 600;

/* URL clicker options */
static char *url_opener = "xdg-open"; /* Application to open the clicked URL in */

/*
 * Default colour and shape of the mouse cursor
 */
static unsigned int mouseshape = XC_xterm;
static unsigned int mousefg = 7;
static unsigned int mousebg = 0;

/* Execution options */
static char *shell = "/bin/sh"; /* Shell to execute with the '-e' (execute command) flag. */
char *stty_args = "stty raw pass8 nl -echo -iexten -cstopb 38400";

/* Character bounding-box multipliers */
static float cwscale = 1.0;
static float chscale = 1.0;

/* Color options
 *
 * Regular colors (1-8)
 * Note that these can also be set in .Xresources, despite being #defined here.
 */
#define col_1   "#5c5c5c" /* regular color 1 */
#define col_2   "#e57373" /* regular color 2 */
#define col_3   "#02982e" /* regular color 3 */
#define col_4   "#fac863" /* regular color 4 */
#define col_5   "#6699cc" /* regular color 5 */
#define col_6   "#a36ac7" /* regular color 6 */
#define col_7   "#5fb3b3" /* regular color 7 */
#define col_8   "#c0c5ce" /* regular color 8 */

/* Bright colors (9-16) */
#define col_9   "#00ffaa" /* bright color 1 */
#define col_10  "#e57373" /* bright color 2 */
#define col_11  "#a6bc69" /* bright color 3 */
#define col_12  "#6699cc" /* bright color 4 */
#define col_13  "#5fb3b3" /* bright color 5 */
#define col_14  "#c594c5" /* bright color 6 */
#define col_15  "#cccccc" /* bright color 7 */
#define col_16  "#555555" /* bright color 8 */

/* Alpha options */
float alpha = 0.8;

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 258; /* Foreground */
unsigned int defaultbg = 259; /* Background */
unsigned int defaultcs = 256; /* Cursor */
unsigned int defaultrc = 257; /* Reverse cursor */
unsigned int defaultattr = 11; /* Invalid font returned by fontconfig */

/* Cursor options
 *
 * Thickness of underline and bar cursors
 */
static unsigned int cursorthickness = 2;
static unsigned int blinktimeout = 800; /* Timeout in milliseconds between cursor blink.
										 * Setting this to 0 will disable the blinking cursor!
										 */
/*
 * 0: Blinking block
 * 1: Blinking block (default)
 * 2: Steady block ("█")
 * 3: Blinking underline
 * 4: Steady underline ("_")
 * 5: Blinking bar
 * 6: Steady bar ("|")
 * 7: Blinking ccursor cursor
 * 8: Steady ccursor cursor
 */
static unsigned int cursorstyle = 1; /* Style to draw the cursor in */
static Rune ccursor            = 0x2603; /* Snowman ("☃") */

/* Boxdraw options
 *
 * 1: Render most of the lines/blocks characters without using the font for
 *    perfect alignment between cells (U2500 - U259F except dashes/diagonals).
 *    Bold affects lines thickness if boxdraw_bold is not 0. Italic is ignored.
 * 0: Disable boxdraw (render all U25XX glyphs normally from the font).
 */
int boxdraw = 1; /* Enable boxdraw */
int boxdraw_bold = 1; /* Draw boxdraw bold */
int boxdraw_braille = 1; /* Render braille as adjecent pixels */

/* Undercurl options
 * 
 * Undercurl allows your terminal and TUI software to draw undercurl instead of an underline.
 * This looks nice to a lot of people.
 *
 * Curly:
 *  _   _   _   _
 * ( ) ( ) ( ) ( )
 *	 (_) (_) (_) (_)
 *
 * To use Curly:
 * Set undercurl to 1
 *
 * Spiky:
 * /\  /\   /\	/\
 *   \/  \/	  \/
 *
 * To use Spiky:
 * Set undercurl to 2
 *
 * Capped:
 *	_     _     _
 * / \   / \   / \
 *    \_/   \_/
 *
 * To use Capped:
 * Set undercurl to 3
 *
 * To use the default (underline), set undercurl to 0.
 */

static char *understyle          = "1"; /* Undercurl style to use */

/* Bell options
 */
static int bellvolume = 0; /* Bell volume value between -100 and 100. 0 will disable it! */

/* Default $TERM value
 *
 * If you find that you need it, you may set this to xterm-256color for extra compatibility
 * Or if you prefer, you may also use no color or 16 color to disable full color support.
 */
char *termname = "st-256color"; /* $TERM value to export */

/* Tab spacing options */
unsigned int tabspaces = 4; /* Spaces per tab */
