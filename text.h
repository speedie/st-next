/* Miscellanious text related options that you probably do not need to change. */

/* Ascii characters used to estimate the advance width of single wide characters */
static char ascii_printable[] =
	" !\"#$%&'()*+,-./0123456789:;<=>?"
	"@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_"
	"`abcdefghijklmnopqrstuvwxyz{|}~";
