/* This header file is for Mouse keybinds.
 *
 * Make sure you don't overload Button1 (Left click).
 *
 * Mouse1 | Left Click
 * Mouse2 | Middle Click
 */

static MouseShortcut mshortcuts[] = {
	/* mask       button   function     argument       release */
	{ XK_NO_MOD,  Button4, kscrollup,   {.i = 1} },
	{ XK_NO_MOD,  Button5, kscrolldown, {.i = 1} },
	{ XK_ANY_MOD, Button2, selpaste,    {.i = 0},      1 },
	{ ShiftMask,  Button4, ttysend,     {.s = "\033[5;2~"} },
	{ XK_ANY_MOD, Button4, ttysend,     {.s = "\031"} },
	{ ShiftMask,  Button5, ttysend,     {.s = "\033[6;2~"} },
	{ XK_ANY_MOD, Button5, ttysend,     {.s = "\005"} },
};

