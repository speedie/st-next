/* st externalpipe
 *
 * Externalpipe allows the user to write scripts that can read the text on the screen and perform actions on it (basically st | <script>).
 *
 * Below you can set the commands that will run, that can be, for example, bound to a key (see keybinds.h)
 */

char *externalpipe_sigusr1[] = { "/bin/sh", "-c", "st_buffer st_strings_read" };
static char *listurl[] = { "/bin/sh", "-c", "st_urllist", NULL };
