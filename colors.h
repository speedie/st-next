/* st color array */

static char *colorname[]      = {
	col_1, /* black */
	col_2, /* red3 */
	col_3, /* green3 */
	col_4, /* yellow3 */
	col_5, /* blue2 */
	col_6, /* magenta3 */
	col_7, /* cyan3 */
	col_8, /* gray90 */
	col_9, /* gray50 */
	col_10, /* red */
	col_11, /* green */
	col_12, /* yellow */
	col_13, /* #5c5cff */
    col_14, /* magenta */
	col_15, /* cyan */
	col_16, /* white */
	[255] = 0,
	"#cccccc",
	"#555555",
	"#c0c5ce",
	"#696969",
};
